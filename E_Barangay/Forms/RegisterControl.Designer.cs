﻿namespace E_Barangay.Forms
{
    partial class RegisterControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegisterControl));
            this.FirstNameField = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Birthday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ContactField = new System.Windows.Forms.TextBox();
            this.NumberField = new System.Windows.Forms.TextBox();
            this.SexOption = new System.Windows.Forms.ComboBox();
            this.isIndigent = new System.Windows.Forms.CheckBox();
            this.IsPwd = new System.Windows.Forms.CheckBox();
            this.IsSenior = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AreaOption = new System.Windows.Forms.ComboBox();
            this.MunicipalityField = new System.Windows.Forms.TextBox();
            this.ResidenceTypeOption = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ProvinceField = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BarangayField = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.IsStudent = new System.Windows.Forms.CheckBox();
            this.VoterCheckbox = new System.Windows.Forms.CheckBox();
            this.RecordsTable = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.AddRecordBtn = new System.Windows.Forms.Button();
            this.RegisterBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.PrecinctNumField = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.VoterIDField = new System.Windows.Forms.TextBox();
            this.CivilStatusOption = new System.Windows.Forms.ComboBox();
            this.AgeField = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.SpouseField = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.MotherField = new System.Windows.Forms.TextBox();
            this.FatherField = new System.Windows.Forms.TextBox();
            this.BdayPicker = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.LastNameField = new System.Windows.Forms.TextBox();
            this.MiddleNameField = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AddImage = new System.Windows.Forms.Button();
            this.ImageBox = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.IDField = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordsTable)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ImageBox)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // FirstNameField
            // 
            this.FirstNameField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.FirstNameField.Location = new System.Drawing.Point(81, 40);
            this.FirstNameField.Name = "FirstNameField";
            this.FirstNameField.Size = new System.Drawing.Size(180, 20);
            this.FirstNameField.TabIndex = 2;
            this.FirstNameField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RegisterControl_KeyDown);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 14);
            this.label1.TabIndex = 4;
            this.label1.Text = "First Name";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 14);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sex";
            // 
            // Birthday
            // 
            this.Birthday.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Birthday.AutoSize = true;
            this.Birthday.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Birthday.Location = new System.Drawing.Point(6, 17);
            this.Birthday.Name = "Birthday";
            this.Birthday.Size = new System.Drawing.Size(56, 14);
            this.Birthday.TabIndex = 6;
            this.Birthday.Text = "Birth Date";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 14);
            this.label3.TabIndex = 7;
            this.label3.Text = "Contact";
            // 
            // ContactField
            // 
            this.ContactField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ContactField.Location = new System.Drawing.Point(68, 40);
            this.ContactField.MaxLength = 11;
            this.ContactField.Name = "ContactField";
            this.ContactField.Size = new System.Drawing.Size(231, 20);
            this.ContactField.TabIndex = 14;
            // 
            // NumberField
            // 
            this.NumberField.Location = new System.Drawing.Point(86, 14);
            this.NumberField.Name = "NumberField";
            this.NumberField.Size = new System.Drawing.Size(73, 20);
            this.NumberField.TabIndex = 4;
            // 
            // SexOption
            // 
            this.SexOption.BackColor = System.Drawing.Color.White;
            this.SexOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SexOption.FormattingEnabled = true;
            this.SexOption.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.SexOption.Location = new System.Drawing.Point(68, 67);
            this.SexOption.Name = "SexOption";
            this.SexOption.Size = new System.Drawing.Size(93, 21);
            this.SexOption.TabIndex = 17;
            // 
            // isIndigent
            // 
            this.isIndigent.AutoSize = true;
            this.isIndigent.Location = new System.Drawing.Point(6, 19);
            this.isIndigent.Name = "isIndigent";
            this.isIndigent.Size = new System.Drawing.Size(70, 17);
            this.isIndigent.TabIndex = 9;
            this.isIndigent.Text = "Indigent";
            this.isIndigent.UseVisualStyleBackColor = true;
            // 
            // IsPwd
            // 
            this.IsPwd.AutoSize = true;
            this.IsPwd.Location = new System.Drawing.Point(6, 42);
            this.IsPwd.Name = "IsPwd";
            this.IsPwd.Size = new System.Drawing.Size(53, 17);
            this.IsPwd.TabIndex = 10;
            this.IsPwd.Text = "PWD";
            this.IsPwd.UseVisualStyleBackColor = true;
            // 
            // IsSenior
            // 
            this.IsSenior.AutoSize = true;
            this.IsSenior.Location = new System.Drawing.Point(6, 65);
            this.IsSenior.Name = "IsSenior";
            this.IsSenior.Size = new System.Drawing.Size(104, 17);
            this.IsSenior.TabIndex = 11;
            this.IsSenior.Text = "Senior Citizen";
            this.IsSenior.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.AreaOption);
            this.groupBox1.Controls.Add(this.MunicipalityField);
            this.groupBox1.Controls.Add(this.ResidenceTypeOption);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ProvinceField);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.BarangayField);
            this.groupBox1.Controls.Add(this.NumberField);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(391, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 126);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address";
            // 
            // AreaOption
            // 
            this.AreaOption.FormattingEnabled = true;
            this.AreaOption.Location = new System.Drawing.Point(41, 40);
            this.AreaOption.Name = "AreaOption";
            this.AreaOption.Size = new System.Drawing.Size(118, 21);
            this.AreaOption.TabIndex = 19;
            // 
            // MunicipalityField
            // 
            this.MunicipalityField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MunicipalityField.Location = new System.Drawing.Point(251, 40);
            this.MunicipalityField.Name = "MunicipalityField";
            this.MunicipalityField.Size = new System.Drawing.Size(151, 20);
            this.MunicipalityField.TabIndex = 7;
            // 
            // ResidenceTypeOption
            // 
            this.ResidenceTypeOption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ResidenceTypeOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ResidenceTypeOption.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.ResidenceTypeOption.FormattingEnabled = true;
            this.ResidenceTypeOption.Items.AddRange(new object[] {
            "Resident",
            "Transient"});
            this.ResidenceTypeOption.Location = new System.Drawing.Point(251, 66);
            this.ResidenceTypeOption.Name = "ResidenceTypeOption";
            this.ResidenceTypeOption.Size = new System.Drawing.Size(151, 21);
            this.ResidenceTypeOption.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(179, 69);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 14);
            this.label18.TabIndex = 22;
            this.label18.Text = "Type\r\n";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Province";
            // 
            // ProvinceField
            // 
            this.ProvinceField.Location = new System.Drawing.Point(61, 66);
            this.ProvinceField.Name = "ProvinceField";
            this.ProvinceField.Size = new System.Drawing.Size(98, 20);
            this.ProvinceField.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(179, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 14);
            this.label7.TabIndex = 19;
            this.label7.Text = "Municipality";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(179, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 14);
            this.label6.TabIndex = 18;
            this.label6.Text = "Barangay";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Area";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "House Number";
            // 
            // BarangayField
            // 
            this.BarangayField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BarangayField.Location = new System.Drawing.Point(251, 14);
            this.BarangayField.Name = "BarangayField";
            this.BarangayField.Size = new System.Drawing.Size(151, 20);
            this.BarangayField.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.IsStudent);
            this.groupBox2.Controls.Add(this.isIndigent);
            this.groupBox2.Controls.Add(this.IsPwd);
            this.groupBox2.Controls.Add(this.IsSenior);
            this.groupBox2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(526, 296);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(279, 155);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Status";
            // 
            // IsStudent
            // 
            this.IsStudent.AutoSize = true;
            this.IsStudent.Location = new System.Drawing.Point(135, 19);
            this.IsStudent.Name = "IsStudent";
            this.IsStudent.Size = new System.Drawing.Size(67, 17);
            this.IsStudent.TabIndex = 12;
            this.IsStudent.Text = "Student";
            this.IsStudent.UseVisualStyleBackColor = true;
            // 
            // VoterCheckbox
            // 
            this.VoterCheckbox.AutoSize = true;
            this.VoterCheckbox.Location = new System.Drawing.Point(9, 94);
            this.VoterCheckbox.Name = "VoterCheckbox";
            this.VoterCheckbox.Size = new System.Drawing.Size(54, 17);
            this.VoterCheckbox.TabIndex = 20;
            this.VoterCheckbox.Text = "Voter";
            this.VoterCheckbox.UseVisualStyleBackColor = true;
            this.VoterCheckbox.CheckedChanged += new System.EventHandler(this.VoterCheckbox_CheckedChanged);
            // 
            // RecordsTable
            // 
            this.RecordsTable.AllowUserToAddRows = false;
            this.RecordsTable.AllowUserToDeleteRows = false;
            this.RecordsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RecordsTable.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.RecordsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.RecordsTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column3,
            this.Column2});
            this.RecordsTable.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.RecordsTable.Location = new System.Drawing.Point(3, 24);
            this.RecordsTable.MultiSelect = false;
            this.RecordsTable.Name = "RecordsTable";
            this.RecordsTable.ReadOnly = true;
            this.RecordsTable.RowHeadersVisible = false;
            this.RecordsTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.RecordsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.RecordsTable.Size = new System.Drawing.Size(502, 143);
            this.RecordsTable.TabIndex = 21;
            this.RecordsTable.TabStop = false;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Date";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Title";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 200;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.HeaderText = "Details";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.AddRecordBtn);
            this.groupBox3.Controls.Add(this.RecordsTable);
            this.groupBox3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(9, 296);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(511, 202);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Records";
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.BackColor = System.Drawing.Color.Silver;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(73, 173);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 23);
            this.button4.TabIndex = 23;
            this.button4.TabStop = false;
            this.button4.Text = "Delete Record";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // AddRecordBtn
            // 
            this.AddRecordBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AddRecordBtn.BackColor = System.Drawing.Color.Silver;
            this.AddRecordBtn.FlatAppearance.BorderSize = 0;
            this.AddRecordBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddRecordBtn.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddRecordBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddRecordBtn.Location = new System.Drawing.Point(3, 173);
            this.AddRecordBtn.Name = "AddRecordBtn";
            this.AddRecordBtn.Size = new System.Drawing.Size(70, 23);
            this.AddRecordBtn.TabIndex = 22;
            this.AddRecordBtn.TabStop = false;
            this.AddRecordBtn.Text = "Add Record";
            this.AddRecordBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.AddRecordBtn.UseVisualStyleBackColor = false;
            this.AddRecordBtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // RegisterBtn
            // 
            this.RegisterBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RegisterBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.RegisterBtn.FlatAppearance.BorderSize = 2;
            this.RegisterBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RegisterBtn.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RegisterBtn.Image = ((System.Drawing.Image)(resources.GetObject("RegisterBtn.Image")));
            this.RegisterBtn.Location = new System.Drawing.Point(526, 457);
            this.RegisterBtn.Name = "RegisterBtn";
            this.RegisterBtn.Size = new System.Drawing.Size(111, 41);
            this.RegisterBtn.TabIndex = 6;
            this.RegisterBtn.Text = "Register";
            this.RegisterBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.RegisterBtn.UseVisualStyleBackColor = false;
            this.RegisterBtn.Click += new System.EventHandler(this.RegisterBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.CancelBtn.FlatAppearance.BorderSize = 2;
            this.CancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CancelBtn.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("CancelBtn.Image")));
            this.CancelBtn.Location = new System.Drawing.Point(642, 456);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(95, 42);
            this.CancelBtn.TabIndex = 7;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CancelBtn.UseVisualStyleBackColor = false;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.PrecinctNumField);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.VoterCheckbox);
            this.groupBox4.Controls.Add(this.VoterIDField);
            this.groupBox4.Controls.Add(this.CivilStatusOption);
            this.groupBox4.Controls.Add(this.AgeField);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.SpouseField);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.MotherField);
            this.groupBox4.Controls.Add(this.FatherField);
            this.groupBox4.Controls.Add(this.BdayPicker);
            this.groupBox4.Controls.Add(this.SexOption);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.Birthday);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.ContactField);
            this.groupBox4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(9, 152);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(796, 138);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Basic Info";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(305, 97);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 14);
            this.label17.TabIndex = 1;
            this.label17.Text = "Precinct Number";
            // 
            // PrecinctNumField
            // 
            this.PrecinctNumField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PrecinctNumField.Enabled = false;
            this.PrecinctNumField.Location = new System.Drawing.Point(396, 94);
            this.PrecinctNumField.Name = "PrecinctNumField";
            this.PrecinctNumField.Size = new System.Drawing.Size(388, 20);
            this.PrecinctNumField.TabIndex = 22;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(69, 97);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 14);
            this.label16.TabIndex = 0;
            this.label16.Text = "Voter\'s ID";
            // 
            // VoterIDField
            // 
            this.VoterIDField.Enabled = false;
            this.VoterIDField.Location = new System.Drawing.Point(128, 94);
            this.VoterIDField.Name = "VoterIDField";
            this.VoterIDField.Size = new System.Drawing.Size(171, 20);
            this.VoterIDField.TabIndex = 21;
            // 
            // CivilStatusOption
            // 
            this.CivilStatusOption.BackColor = System.Drawing.Color.White;
            this.CivilStatusOption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CivilStatusOption.FormattingEnabled = true;
            this.CivilStatusOption.Items.AddRange(new object[] {
            "Single",
            "Married"});
            this.CivilStatusOption.Location = new System.Drawing.Point(232, 66);
            this.CivilStatusOption.Name = "CivilStatusOption";
            this.CivilStatusOption.Size = new System.Drawing.Size(67, 21);
            this.CivilStatusOption.TabIndex = 18;
            this.CivilStatusOption.SelectedIndexChanged += new System.EventHandler(this.CivilStatusOption_SelectedIndexChanged);
            // 
            // AgeField
            // 
            this.AgeField.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AgeField.Location = new System.Drawing.Point(263, 17);
            this.AgeField.MaxLength = 11;
            this.AgeField.Name = "AgeField";
            this.AgeField.ReadOnly = true;
            this.AgeField.Size = new System.Drawing.Size(36, 13);
            this.AgeField.TabIndex = 14;
            this.AgeField.TabStop = false;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(323, 70);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 14);
            this.label14.TabIndex = 26;
            this.label14.Text = "Spouse Name";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(232, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 14);
            this.label15.TabIndex = 27;
            this.label15.Text = "Age";
            // 
            // SpouseField
            // 
            this.SpouseField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SpouseField.Enabled = false;
            this.SpouseField.Location = new System.Drawing.Point(397, 66);
            this.SpouseField.MaxLength = 100;
            this.SpouseField.Name = "SpouseField";
            this.SpouseField.Size = new System.Drawing.Size(387, 20);
            this.SpouseField.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(167, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 14);
            this.label13.TabIndex = 23;
            this.label13.Text = "Civil Status";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(313, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 14);
            this.label12.TabIndex = 22;
            this.label12.Text = "Mother\'s Name";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(318, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 14);
            this.label11.TabIndex = 21;
            this.label11.Text = "Father\'s Name";
            // 
            // MotherField
            // 
            this.MotherField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MotherField.Location = new System.Drawing.Point(396, 40);
            this.MotherField.Name = "MotherField";
            this.MotherField.Size = new System.Drawing.Size(388, 20);
            this.MotherField.TabIndex = 16;
            // 
            // FatherField
            // 
            this.FatherField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FatherField.Location = new System.Drawing.Point(397, 14);
            this.FatherField.Name = "FatherField";
            this.FatherField.Size = new System.Drawing.Size(387, 20);
            this.FatherField.TabIndex = 15;
            // 
            // BdayPicker
            // 
            this.BdayPicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.BdayPicker.Checked = false;
            this.BdayPicker.CustomFormat = "";
            this.BdayPicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.BdayPicker.Location = new System.Drawing.Point(68, 14);
            this.BdayPicker.Name = "BdayPicker";
            this.BdayPicker.Size = new System.Drawing.Size(158, 20);
            this.BdayPicker.TabIndex = 13;
            this.BdayPicker.Value = new System.DateTime(2019, 11, 10, 0, 0, 0, 0);
            this.BdayPicker.ValueChanged += new System.EventHandler(this.BdayPicker_ValueChanged);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(7, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 14);
            this.label10.TabIndex = 17;
            this.label10.Text = "Last Name";
            // 
            // LastNameField
            // 
            this.LastNameField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.LastNameField.Location = new System.Drawing.Point(81, 92);
            this.LastNameField.Name = "LastNameField";
            this.LastNameField.Size = new System.Drawing.Size(180, 20);
            this.LastNameField.TabIndex = 4;
            // 
            // MiddleNameField
            // 
            this.MiddleNameField.Location = new System.Drawing.Point(81, 66);
            this.MiddleNameField.Name = "MiddleNameField";
            this.MiddleNameField.Size = new System.Drawing.Size(180, 20);
            this.MiddleNameField.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(7, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 14);
            this.label9.TabIndex = 14;
            this.label9.Text = "Middle Name";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.AddImage);
            this.panel1.Controls.Add(this.ImageBox);
            this.panel1.Location = new System.Drawing.Point(9, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(103, 105);
            this.panel1.TabIndex = 18;
            // 
            // AddImage
            // 
            this.AddImage.FlatAppearance.BorderSize = 0;
            this.AddImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddImage.Image = ((System.Drawing.Image)(resources.GetObject("AddImage.Image")));
            this.AddImage.Location = new System.Drawing.Point(0, -2);
            this.AddImage.Name = "AddImage";
            this.AddImage.Size = new System.Drawing.Size(27, 27);
            this.AddImage.TabIndex = 1;
            this.AddImage.TabStop = false;
            this.AddImage.UseVisualStyleBackColor = true;
            this.AddImage.Click += new System.EventHandler(this.AddImage_Click);
            // 
            // ImageBox
            // 
            this.ImageBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ImageBox.Location = new System.Drawing.Point(-37, -1);
            this.ImageBox.Name = "ImageBox";
            this.ImageBox.Size = new System.Drawing.Size(172, 105);
            this.ImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImageBox.TabIndex = 0;
            this.ImageBox.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.IDField);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.FirstNameField);
            this.groupBox5.Controls.Add(this.LastNameField);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.MiddleNameField);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(118, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(267, 126);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Name";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(6, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 14);
            this.label19.TabIndex = 19;
            this.label19.Text = "ID";
            // 
            // IDField
            // 
            this.IDField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.IDField.Location = new System.Drawing.Point(81, 14);
            this.IDField.MaxLength = 10;
            this.IDField.Name = "IDField";
            this.IDField.Size = new System.Drawing.Size(180, 20);
            this.IDField.TabIndex = 1;
            // 
            // RegisterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.RegisterBtn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Name = "RegisterControl";
            this.Size = new System.Drawing.Size(819, 501);
            this.Load += new System.EventHandler(this.RegisterControl_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RecordsTable)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ImageBox)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox FirstNameField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Birthday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ContactField;
        private System.Windows.Forms.TextBox NumberField;
        private System.Windows.Forms.ComboBox SexOption;
        private System.Windows.Forms.CheckBox isIndigent;
        private System.Windows.Forms.CheckBox IsPwd;
        private System.Windows.Forms.CheckBox IsSenior;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BarangayField;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView RecordsTable;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button RegisterBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker BdayPicker;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox ProvinceField;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox LastNameField;
        private System.Windows.Forms.TextBox MiddleNameField;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button AddRecordBtn;
        private System.Windows.Forms.CheckBox IsStudent;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox MotherField;
        private System.Windows.Forms.TextBox FatherField;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox SpouseField;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox AgeField;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox CivilStatusOption;
        private System.Windows.Forms.PictureBox ImageBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Button AddImage;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox PrecinctNumField;
        private System.Windows.Forms.TextBox VoterIDField;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox ResidenceTypeOption;
        private System.Windows.Forms.CheckBox VoterCheckbox;
        private System.Windows.Forms.TextBox MunicipalityField;
        private System.Windows.Forms.ComboBox AreaOption;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox IDField;
    }
}
