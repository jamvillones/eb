//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace E_Barangay
{
    using System;
    using System.Collections.Generic;
    
    public partial class Record
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string CitizenID { get; set; }
        public string Details { get; set; }
        public Nullable<System.DateTime> DateIssued { get; set; }
    
        public virtual Citizen Citizen { get; set; }
    }
}
