﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_Barangay.Interface
{
    public interface IRecordAcceptor
    {
        void AcceptRecord(Record r);
    }
}
